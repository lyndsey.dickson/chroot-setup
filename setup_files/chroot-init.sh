#!/bin/bash

non_root_user=user

while getopts "u:" arg; do
	case ${arg} in
		u)
			non_root_user=${OPTARG}
			;;
	esac
done

# Install base packages
sed -i 's/^CheckSpace/#CheckSpace/g' /etc/pacman.conf
pacman -S --noconfirm vim base-devel

# Create the non root user
useradd -m $non_root_user

# Add non root user to sudoers
sed -i "/root ALL=(ALL) ALL/a $non_root_user ALL=(ALL) NOPASSWD: ALL" /etc/sudoers

# Get the directory of the script file
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# install yay
chmod +x ${DIR}/install-yay.sh
cp ${DIR}/install-yay.sh /home/${non_root_user}
chown ${non_root_user}:${non_root_user} /home/${non_root_user}/install-yay.sh
sudo -u ${non_root_user} /home/${non_root_user}/install-yay.sh
rm /home/${non_root_user}/install-yay.sh
