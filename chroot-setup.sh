#!/bin/bash

non_root_user=user

# Get the command line arguments
while getopts "n:u:" arg; do
	case ${arg} in
		n)
			chroot_name=${OPTARG}
			;;
		u)
			non_root_user=${OPTARG}
			;;
	esac
done

# Make sure a chroot name was passed
if [ -z ${chroot_name} ]; then
	echo "Chroot name required"
	exit
fi

# Get the directory of the script file
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
	DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
	SOURCE="$(readlink "$SOURCE")"
	[[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# setup the chroot
mkarchroot $chroot_name base
sudo cp ${DIR}/setup_files/* ${chroot_name}/root
sudo chmod +x ${chroot_name}/root/chroot-init.sh
sudo arch-chroot $chroot_name /root/chroot-init.sh -u $non_root_user
