
# Chroot Setup

I created this set of bash scripts because I got tired of installing a bunch of random stuff on my system to test it out. Running things in docker is great sometimes, but often overkill, so now I'm learning to setup a chroot.

These bash scripts are useable but also serve as a personal reference.

I'm not sure if I'm doing things the right way. Please create an issue if you know a better way of doing things.

# Setting up a chroot

[Creating a chrooted build environment](https://rxos.readthedocs.io/en/develop/appendices/chroot_environment.html)

# Requirements

I designed this to work in Arch Linux. It is a beautiful Linux distro created by the community to be user-friendly, despite what some people may say about it.

Required packages arch packages:

- arch-install-scripts
- devtools

# Usage

Run chroot-setup.sh with what your chroot to be named:
 
    chroot-setup.sh -n chroot_name
